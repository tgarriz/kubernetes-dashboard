# Kubernetes Dashboard

### Instalar y configurar kubernetes Dashboard con certificados autofirmados 
```bash
helm repo add kubernetes-dashboard https://kubernetes.github.io/dashboard/
```
### Bajar el values y configurar
```bash
wget https://raw.githubusercontent.com/kubernetes/dashboard/master/charts/kubernetes-dashboard/values.yaml
```
Dejamos el false el ingress ya que lo formamos nosotros luego de crear el certificado

### Instalamos
```bash
helm upgrade --install kubernetes-dashboard kubernetes-dashboard/kubernetes-dashboard --create-namespace --namespace kubernetes-dashboard
```
### Creamos certificado y secret
```bash
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout dashboard.key -out dashboard.crt -subj "/CN=dash.mycluster.com/O=dash.mycluster.com"
kubectl create secret tls dashboard-tls --key dashboard.key --cert dashboard.crt -n kubernetes-dashboard
```
### Ahora creamos el ingress
```yaml
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  annotations:
    nginx.ingress.kubernetes.io/backend-protocol: HTTPS
    nginx.ingress.kubernetes.io/ssl-passthrough: "true"
    nginx.ingress.kubernetes.io/ssl-redirect: "true"
  name: dashboard
  namespace: kubernetes-dashboard
spec:
  ingressClassName: nginx
  rules:
  - host: dash.mycluster.com
    http:
      paths:
      - backend:
          service:
            name: kubernetes-dashboard-kong-proxy
            port:
              number: 443
        path: /
        pathType: Prefix
  tls:
  - hosts:
    - dash.mycluster.com
    secretName: dashboard-tls
```
No es la unica forma de instalar, tambien podriamos crear el certificado inicialmente y luego especificarlo en el values y pasar a true la instalacion del ingress, o bien dejar todo en manos de cert-manager, pero para esta ultima habria que exponer el dominio a certificar a internet ya que es condicion de letsencrypt.

### Creamos el serviceaccount, clusterrolebinding y por ultimo un token de autenticacion

```yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kubernetes-dashboard
---
apiVersion: v1
kind: Secret
metadata:
  name: admin-user
  namespace: kubernetes-dashboard
  annotations:
    kubernetes.io/service-account.name: "admin-user"
type: kubernetes.io/service-account-token
```
Para ver el token
```bash
kubectl get secret admin-user -n kubernetes-dashboard -o jsonpath={".data.token"} | base64 -d
```
